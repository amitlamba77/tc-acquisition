import React from 'react';

import InfiniteCarousel from 'react-leaf-carousel';

class WelcomeOffer extends React.Component {
    render() {
        // var { offerDesc, logoImage, offerName, offerImage } = this.props;
        return (
            <InfiniteCarousel
                breakpoints={[
                    {
                        breakpoint: 500,
                        settings: {
                            slidesToShow: 1.2,
                            slidesToScroll: 1,
                        },
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 0,
                        },
                    }
                ]}
                sidesOpacity={0}
                sideSize={0}
                slidesToScroll={3}
                slidesToShow={3}
                scrollOnDevice={true}
                swipe={true}
                margin={0}
            >
                <div className='item'>


                    <div className="welcome_offer_block welcome_offer_1 rel-pos">
                        <div className="head">
                            <div className="offer_details">

                                <div className="desc">Get 25% off on Uber Premier</div>
                                <div className="logo_offer">
                                    <img src={require('./images/uber.png')} alt="Welcome Offer" />
                                </div>
                                <div>Welcome Offer</div>
                                <div className="clearfix"></div>
                            </div>
                            <div className="logo_right">
                                <img src={require('./images/offer_image.png')} alt="Welcome Offer" />
                            </div>
                            <div className="clearfix"></div>
                        </div>

                    </div>
                </div>
                <div className='item'>
                    <div className="welcome_offer_block welcome_offer_2 rel-pos">
                        <div className="head">
                            <div className="offer_details">

                                <div className="desc">Get Flat 20% off on annual subscription of Sony LIV</div>
                                <div className="logo_offer">
                                    <img src={require('./images/sony_liv.png')} alt="Welcome Offer" />
                                </div>
                                <div>Welcome Offer</div>
                                <div className="clearfix"></div>
                            </div>
                            <div className="logo_right">
                                <img src={require('./images/offer_image2.png')} alt="Welcome Offer" />
                            </div>
                            <div className="clearfix"></div>
                        </div>

                    </div>
                </div>
                <div className='item'>
                    <div className="welcome_offer_block welcome_offer_3 rel-pos">
                        <div className="head">
                            <div className="offer_details">

                                <div className="desc">50% off on men's personal care products</div>
                                <div className="logo_offer">
                                    <img src={require('./images/mens_xp.png')} alt="Welcome Offer" />
                                </div>
                                <div>Welcome Offer</div>
                                <div className="clearfix"></div>
                            </div>
                            <div className="logo_right">
                                <img src={require('./images/offer_image3.png')} alt="Welcome Offer" />
                            </div>
                            <div className="clearfix"></div>
                        </div>

                    </div>



                    {/* <div className="welcome_offer_block welcome_offer_3 rel-pos">
                        <div className="head">
                            <div className="offer_details">

                                <div className="desc">{offerDesc}</div>
                                <div className="logo_offer">
                                    <img src={require('./images/' + logoImage)} />
                                </div>
                                <div>{offerName}</div>
                                <div className="clearfix"></div>
                            </div>
                            <div className="logo_right">
                                <img src={require('./images/' + offerImage)} />
                            </div>
                            <div className="clearfix"></div>
                        </div>

                    </div> */}
                </div>






            </InfiniteCarousel>
        )
    }
}



export default WelcomeOffer;