import React from 'react';
class TopOffers extends React.Component {
    render() {
        var { image, topic } = this.props;
        return (
            <div className="offer_listing">
                <div className="float-left">
                    <img src={require('./images/' + image)} alt='Times Card' />
                </div>
                <div className="float-left list_desc">
                    <p>{topic}</p>
                </div>
                <div className="clearfix"></div>
            </div>
        );
    }
}

export default TopOffers;