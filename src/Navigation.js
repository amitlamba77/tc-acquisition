import React from 'react';
import './sass/navigation.scss';
import { Link } from 'react-router-dom';



class Navigation extends React.Component {
    render() {

        return (
            <nav className="header_nav" >

                <div className="d-none d-sm-block">
                    <Link to='/'>
                        HOME
                    </Link>
                    <Link to='/membership'>
                        MEMBERSHIPS
                            </Link>
                    <Link to='/offers'>
                        OFFERS
                            </Link>
                    <Link to='/offers'>
                        Events
                            </Link>
                </div>
                <div className="mobile_logo">
                    <div className="tc_logo d-block d-sm-none float-left">
                        <img src={require("./images/tc_logo.svg")} alt="Times card" />
                    </div>
                    <div className="float-right login_btn">
                        <div className="float-left">
                            <div>Already have a card?</div>
                            <a href="home">LOGIN</a>
                        </div>
                        <div className="float-right user_icon">
                            <img src={require('./images/icon-user.svg')} alt="User" />
                        </div>
                        <div className="clearfix"></div>
                    </div>
                    <div className="clearfix"></div>
                </div>
                <div className="clearfix"></div>
            </nav>

        )
    }
}

export default Navigation;