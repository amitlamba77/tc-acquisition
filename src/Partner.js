import React from 'react';
import Navigation from './Navigation';
import Breadcrumbs from './Breadcrumb';
import Footer from './Footer';
import { MDBInput, MDBBtn } from "mdbreact";


class Partner extends React.Component {
    state = {
        fullname: ""
    };


    submitHandler = event => {
        event.preventDefault();
        event.target.className += " was-validated";
    };

    changeHandler = event => {
        this.setState({ [event.target.name]: event.target.value });
    };
    render() {

        return (
            <div className="generic_pages">
                <div className="ntb_page offer_page sales_page">
                    <div className="form_plan_new form_plan">
                        <div className="top_card_block">
                            <div className="card_block">
                                <Navigation />
                                <div className="title_header">
                                    <div className="card_container">
                                        <div className="">
                                            <Breadcrumbs />
                                            <h1>Partner With Us</h1>
                                        </div>
                                    </div>
                                </div>
                                <div className="page_content">
                                    <div className="card_container">
                                        <div className="title_content">
                                            <div className="cont">
                                                To become a merchant partner for Times Card and bring offers on board, please provide us following details (all the fields are mandatory):

                                            </div>
                                        </div>
                                        <form className="needs-validation"
                                            onSubmit={this.submitHandler}
                                            noValidate>
                                            <div className="">
                                                <div className="card_form_box form_box">
                                                    <div className="row">
                                                        <div className="col-12 col-md-6">
                                                            <div className="form-group">
                                                                <MDBInput outline
                                                                    value={this.state.fullname}
                                                                    name="fullname"
                                                                    onChange={this.changeHandler}
                                                                    type="text"
                                                                    id="fullname"
                                                                    label="Full name"
                                                                    required


                                                                >

                                                                    <div className="invalid-feedback">Please enter your Full Name</div>
                                                                </MDBInput>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-md-6">
                                                            <div className="form-group">
                                                                <MDBInput outline
                                                                    value={this.state.mobilenumber}
                                                                    name="mobilenumber"
                                                                    onChange={this.changeHandler}
                                                                    type="number"
                                                                    id="mobilenumber"
                                                                    label="Mobile Number"
                                                                    required
                                                                >

                                                                    <div className="invalid-feedback">Please enter your  Mobile Number</div>
                                                                </MDBInput>
                                                            </div>
                                                        </div>
                                                        <div className="col-12 col-md-6">
                                                            <div className="form-group">
                                                                <MDBInput outline
                                                                    value={this.state.email}
                                                                    name="email"
                                                                    onChange={this.changeHandler}
                                                                    type="email"
                                                                    id="email"
                                                                    label="Email address"
                                                                    required
                                                                >

                                                                    <div className="invalid-feedback">Please enter your email id</div>
                                                                </MDBInput>
                                                            </div>
                                                        </div>

                                                        <div className="col-12 col-md-6">
                                                            <div className="form-group">
                                                                <select className="browser-default custom-select">
                                                                    <option>City</option>
                                                                    <option value="1">Option 1</option>
                                                                    <option value="2">Option 1</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div className="col-12 col-md-6">
                                                            <div className="form-group">
                                                                <MDBInput outline
                                                                    value={this.state.companyname}
                                                                    name="companyname"
                                                                    onChange={this.changeHandler}
                                                                    type="text"
                                                                    id="companyname"
                                                                    label="Company Name"
                                                                    required
                                                                >

                                                                    <div className="invalid-feedback">Please enter your Company Name</div>
                                                                </MDBInput>
                                                            </div>
                                                        </div>


                                                        <div className="col-12 col-md-6">
                                                            <div className="form-group">
                                                                <select className="browser-default custom-select">
                                                                    <option>Select Offer Category</option>
                                                                    <option value="1">Option 1</option>
                                                                    <option value="2">Option 1</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>



                                                <div className="grn_btn btn_consent form_button">
                                                    <div className="accept_agree accept_agree_bottom ">
                                                        <label className="container">

                                                            <input type="checkbox" checked="checked" />
                                                            <span className="checkmark"></span>
                                                        </label>
                                                        <div>Above information is accurate and correct as per best of my knowledge.</div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-4 col-md-3">
                                                            <MDBBtn color="primary" type="submit" size="lg" style={{ marginLeft: "24px" }} block>
                                                                Submit ›
                                                            </MDBBtn>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>


                                        </form>




                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <Footer className="generic_footer" />
                </div>
            </div>
        )
    }
}

export default Partner;