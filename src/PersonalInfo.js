import React from 'react';
import './App.css';
import './sass/form.scss';
import { MDBInput, MDBBtn } from "mdbreact";
import { Link } from 'react-router-dom';

class PersonalInfo extends React.Component {

    state = {
        fname: "",
        lname: ""
    };


    submitHandler = event => {
        event.preventDefault();
        event.target.className += " was-validated";
    };

    changeHandler = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        return (

            <div className="blur_bg">
                <div className="ntb_page">
                    <div className="white_common_bg center_box center_box_big">
                        <form className="needs-validation"
                            onSubmit={this.submitHandler}
                            noValidate>
                            <div className="form_plan_new form_plan">
                                <div className="head ">
                                    <h1>Personal Information</h1>
                                    <p style={{ marginTop: "-25px" }}><span>+91 9829000000</span> <img src={require('./images/number_check.svg')} alt="Check" />

                                    </p>
                                    <p>Please help us with a few more details!</p>
                                </div>


                                <div className="card_form_box form_box">

                                    <div className="row">
                                        <div className="col-4">
                                            <div className="form-group">
                                                <select className="browser-default custom-select">
                                                    <option value="1">Mr.</option>
                                                    <option value="2">Mrs.</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-6">
                                            <div className="form-group">
                                                <MDBInput outline
                                                    value={this.state.fname}
                                                    name="fname"
                                                    onChange={this.changeHandler}
                                                    type="text"
                                                    id="firstname"
                                                    label="First name"
                                                    required


                                                >

                                                    <div className="invalid-feedback">Please enter your name</div>
                                                </MDBInput>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="form-group">
                                                <MDBInput outline
                                                    value={this.state.lname}
                                                    name="lname"
                                                    onChange={this.changeHandler}
                                                    type="text"
                                                    id="lastname"
                                                    label="Last name"
                                                    required
                                                >

                                                    <div className="invalid-feedback">Please enter your  last name</div>
                                                </MDBInput>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="form-group">
                                                <MDBInput outline
                                                    value={this.state.email}
                                                    name="email"
                                                    onChange={this.changeHandler}
                                                    type="email"
                                                    id="email"
                                                    label="Email address"
                                                    required
                                                >

                                                    <div className="invalid-feedback">Please enter your email id</div>
                                                </MDBInput>
                                            </div>
                                        </div>

                                        <div className="col-6">
                                            <div className="form-group">
                                                <MDBInput outline
                                                    value={this.state.pan}
                                                    name="pan"
                                                    onChange={this.changeHandler}
                                                    type="text"
                                                    id="pan"
                                                    label="PAN Number"
                                                    required
                                                >

                                                    <div className="invalid-feedback">Please enter your Pan Number</div>
                                                </MDBInput>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="form-group">
                                                <MDBInput outline
                                                    value={this.state.pincode}
                                                    name="text"
                                                    onChange={this.changeHandler}
                                                    type="text"
                                                    id="pincode"
                                                    label="Pin Code"
                                                    required
                                                >

                                                    <div className="invalid-feedback">Please enter your Pin Code</div>
                                                </MDBInput>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="form-group">
                                                <MDBInput outline
                                                    value={this.state.dob}
                                                    name="text"
                                                    onChange={this.changeHandler}
                                                    type="text"
                                                    id="dob"
                                                    label="Date of birth"
                                                    required
                                                >

                                                    <div className="invalid-feedback">Please enter your Date of birth</div>
                                                </MDBInput>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="form-group">
                                                <select className="browser-default custom-select">
                                                    <option>Occupation</option>
                                                    <option value="1">Option 1</option>
                                                    <option value="2">Option 1</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="form-group">
                                                <select className="browser-default custom-select">
                                                    <option>Annual Income</option>
                                                    <option value="1">Option 1</option>
                                                    <option value="2">Option 1</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="form-group">
                                                <MDBInput outline
                                                    value={this.state.referral}
                                                    name="referral"
                                                    onChange={this.changeHandler}
                                                    type="text"
                                                    id="referral"
                                                    label="Referral Code (If any)"
                                                    required
                                                >

                                                    <div className="invalid-feedback">Please enter your Referral Code</div>
                                                </MDBInput>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div className="grn_btn btn_consent form_button">
                                    <div className="accept_agree accept_agree_bottom ">
                                        <label className="container">

                                            <input type="checkbox" checked="checked" />
                                            <span className="checkmark"></span>
                                        </label>
                                        <div>Above information is accurate and correct as per best of my knowledge.</div>
                                    </div>
                                    <Link to='/loader'>
                                        <MDBBtn color="primary" type="submit" size="lg" block>
                                            Next ›
                                        </MDBBtn>
                                    </Link>

                                </div>
                            </div>


                        </form>


                    </div>




                </div >

            </div >
        )
    }
}

export default PersonalInfo;