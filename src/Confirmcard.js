import React from 'react';
import './App.css';
import './sass/form.scss';
import './sass/cardconfirm.scss';
import { MDBBtn } from "mdbreact";


class Confirmcard extends React.Component {

    render() {
        return (

            <div className="blur_bg">
                <div className="ntb_page">
                    <div className="white_common_bg center_box ">
                        <form className="needs-validation"
                            onSubmit={this.submitHandler}
                            noValidate>
                            <div className="form_plan_new form_plan">
                                <div className="head ">
                                    <h1>Confirm your card <span>(3/3)</span></h1>
                                </div>


                                <div className="card_form_box form_box">
                                    <div className="row">
                                        <div className="col-12">
                                            <img src={require("./images/platinum.svg")} className="img-fluid" alt="Times Card" />
                                        </div>
                                    </div>

                                    <div class="benefits_pointers">
                                        <div class="pointers">
                                            <span><img src={require("./images/tp.svg")} alt="Benefits" /></span>
                                            <span>save upto ₹ 60000 with TimesPrime</span>
                                        </div>
                                        <div class="pointers">
                                            <span><img src={require("./images/gourmet.svg")} alt="Benefits" /></span>
                                            <span>5% extra cashback with Dineout Pay</span>
                                        </div>
                                    </div>


                                </div>



                                <div className="grn_btn btn_consent form_button">
                                    <div className="accept_agree accept_agree_bottom ">
                                        <label className="container">

                                            <input type="checkbox" checked="checked" />
                                            <span className="checkmark"></span>
                                        </label>
                                        <div>
                                            I confirm that I have read and understood the Product features, Pricing, Terms & Conditions and
                                            the <a href="#" target="_blank">MITC (Most Important Terms & Conditions)</a> applicable to this Product and give my full acceptance to it
                                            . <a href="#" target="_blank">Read more</a>
                                        </div>
                                    </div>
                                    <MDBBtn color="primary" type="submit" size="lg" block>
                                        Submit ›
                                            </MDBBtn>

                                </div>
                            </div>


                        </form>


                    </div>




                </div >

            </div >
        )
    }
}

export default Confirmcard;