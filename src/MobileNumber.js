import React, { Component } from 'react';
import './App.css';
import { MDBInput, MDBBtn } from "mdbreact";
import { Link } from 'react-router-dom';

class Mobilenumber extends Component {
    state = {
        fname: ""
    };


    submitHandler = event => {
        event.preventDefault();
        event.target.className += " was-validated";
    };

    changeHandler = event => {
        this.setState({ [event.target.name]: event.target.value });
    };


    render() {
        return (



            <div className="blur_bg">
                <div className="ntb_page">
                    <div className="white_common_bg center_box">


                        <form className="needs-validation"
                            onSubmit={this.submitHandler}
                            noValidate>


                            <div className="form_plan_new form_plan">
                                <div className="head head_opt">
                                    <h1>Apply for TimesCard</h1>
                                    <p>A new age credit card from the Times Internet & HDFC Bank.</p>
                                    <p>Enter mobile number to check elegibility</p>
                                </div>

                                <div className="card_form_box form_box">
                                    <div className="row">
                                        <div className="col-1">
                                            <div className="form-group font_91">
                                                +91
							                </div>
                                        </div>
                                        <div className="col-10">
                                            <div className="form-group">
                                                <MDBInput outline
                                                    value={this.state.fname}
                                                    name="fname"
                                                    onChange={this.changeHandler}
                                                    type="text"
                                                    id="materialFormRegisterNameEx"
                                                    label="First name"
                                                    required
                                                >

                                                    <div className="invalid-feedback">Please enter your name</div>
                                                </MDBInput>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div className="accept_agree ">
                                    <label className="container">

                                        <input type="checkbox" checked="checked" />
                                        <span className="checkmark"></span>
                                    </label>
                                    <div>I hereby appoint Times Internet as my authorised representative to receive my credit information from experian. Read more</div>
                                </div>

                                <div className="grn_btn btn_consent form_button">
                                    <Link to='/enterotp'>
                                        <MDBBtn color="primary" type="submit" size="lg" block>
                                            Check Elegibility ›
                                    </MDBBtn>
                                    </Link>


                                </div>


                            </div>

                        </form>


                    </div>




                </div>

            </div>

        )
    }
}
export default Mobilenumber;