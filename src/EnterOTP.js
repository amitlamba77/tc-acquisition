import React from 'react';
import './App.css';
import './sass/form.scss';
import { MDBInput, MDBBtn } from "mdbreact";
import { Link } from 'react-router-dom';

class EnterOTP extends React.Component {
    state = {
        otp: ""
    };


    submitHandler = event => {
        event.preventDefault();
        event.target.className += " was-validated";
    };

    changeHandler = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        return (

            <div className="blur_bg">
                <div className="ntb_page">
                    <div className="white_common_bg center_box">
                        <form action="" method="post">
                            <div className="form_plan_new form_plan">
                                <div className="head">
                                    <h1>Enter OTP</h1>
                                    <p>Enter the One Time Password (OTP) sent to your mobile number <span>+91 9829000000</span></p>

                                </div>
                                <div className="otp_page">
                                    <div className="">

                                        <div className="vert_align_abs">

                                            <div className="card_form_box form_box vertical-alignment-helper">
                                                <div className="form-group vertical-align-center ">

                                                    <MDBInput outline
                                                        value={this.state.fname}
                                                        name="otp"
                                                        onChange={this.changeHandler}
                                                        type="text"
                                                        id="materialFormRegisterNameEx"
                                                        label="OTP"
                                                        required
                                                    >

                                                        <div className="invalid-feedback">Please enter OTP</div>
                                                    </MDBInput>


                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                    <div className="action_btn accept_agree">
                                        <a href="#" className="float-left">Didn’t receive OTP?</a>
                                        <a href="#" className="float-right resend_btn">Resend</a>
                                        <div className="clearfix"></div>
                                    </div>

                                    <div className="grn_btn btn_consent form_button">

                                        {/* <button type="button" className="btn btn-lg btn-block action_submit btn-ripple fix_btn">Check Elegibility ›</button> */}
                                        {/* <Button variant="secondary" as="input" size="lg" type="submit" disabled value="proceed ›" block /> */}

                                        <Link to='/personalinfo'>
                                            <MDBBtn color="primary" disabled type="submit" size="lg" block>
                                                proceed ›
                                        </MDBBtn>
                                        </Link>
                                    </div>

                                </div>

                            </div>

                        </form>


                    </div>




                </div>

            </div>
        )
    }
}

export default EnterOTP;