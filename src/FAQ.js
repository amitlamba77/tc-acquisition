import React from 'react';
import Navigation from './Navigation';
import Breadcrumbs from './Breadcrumb';
import Footer from './Footer';
import './sass/cardconfirm.scss';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';



class FaqPage extends React.Component {



    render() {


        return (
            <div className="generic_pages">
                <div className="ntb_page offer_page sales_page">
                    <div className="form_plan_new form_plan">
                        <div className="top_card_block">
                            <div className="card_block">
                                <Navigation />
                                <div className="title_header">
                                    <div className="card_container">
                                        <div className="">
                                            <Breadcrumbs />
                                            <h1>Frequesntly Asked Questions</h1>
                                        </div>
                                    </div>
                                </div>
                                <div className="page_content">
                                    <div className="card_container">
                                        <div className="title_content">
                                            <div className="cont">
                                                To become a merchant partner for Times Card and bring offers on board, please provide us following details (all the fields are mandatory):

                                        </div>
                                            <div className="faq_ques">
                                                <Accordion allowZeroExpanded={true}>
                                                    <AccordionItem>
                                                        <AccordionItemHeading>
                                                            <AccordionItemButton>
                                                                What is Times Card?
                                                            </AccordionItemButton>
                                                        </AccordionItemHeading>
                                                        <AccordionItemPanel>
                                                            <p>
                                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                                                        </AccordionItemPanel>
                                                    </AccordionItem>
                                                    <AccordionItem>
                                                        <AccordionItemHeading>
                                                            <AccordionItemButton>
                                                                How do I apply for a card?
                    </AccordionItemButton>
                                                        </AccordionItemHeading>
                                                        <AccordionItemPanel>
                                                            <p>
                                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                                                        </AccordionItemPanel>
                                                    </AccordionItem>
                                                </Accordion>
                                            </div>
                                        </div>



                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <Footer className="generic_footer" />
                </div>
            </div>
        )
    }
}

export default FaqPage;