import React from 'react';
import './App.css';
import './sass/form.scss';
import { MDBInput, MDBBtn, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";


class Address extends React.Component {

    state = {
        pincode: "",
        radio: 1,
        activeItem: "1"
    };


    submitHandler = event => {
        event.preventDefault();
        event.target.className += " was-validated";
    };

    changeHandler = event => {
        this.setState({ [event.target.name]: event.target.value });
    };


    onClick = (nr) => () => {
        this.setState({
            radio: nr
        });
    }
    toggle = tab => e => {
        if (this.state.activeItem !== tab) {
            this.setState({
                activeItem: tab
            });
        }
    };

    render() {
        return (

            <div className="blur_bg">
                <div className="ntb_page">
                    <div className="white_common_bg center_box">
                        <form className="needs-validation"
                            onSubmit={this.submitHandler}
                            noValidate>
                            <div className="form_plan_new form_plan">
                                <div className="head ">
                                    <h1>Address <span>(2/3)</span></h1>

                                </div>


                                <div className="card_form_box form_box">



                                    <div className="">
                                        <div className="">
                                            <div className="radio_nav_tabs">
                                                <MDBNav className="nav-tabs">

                                                    <MDBNavLink link to="#" active={this.state.activeItem === "1"} onClick={this.toggle("1")} role="tab" >
                                                        <span></span>Home
                                                    </MDBNavLink>
                                                    <MDBNavLink link to="#" active={this.state.activeItem === "2"} onClick={this.toggle("2")} role="tab" >
                                                        <span></span>Residence
                                                    </MDBNavLink>

                                                </MDBNav>
                                                <MDBTabContent activeItem={this.state.activeItem} >
                                                    <MDBTabPane tabId="1" role="tabpanel">
                                                        <div className="row">
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <MDBInput outline
                                                                        value={this.state.pincode}
                                                                        name="pincode"
                                                                        onChange={this.changeHandler}
                                                                        type="text"
                                                                        id="pincode"
                                                                        label="Pin Code"
                                                                        required
                                                                    >

                                                                        <div className="invalid-feedback">Please enter Pin Code</div>
                                                                    </MDBInput>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <MDBInput outline type="textarea" label="Material textarea" rows="5" />

                                                                    <div className="invalid-feedback">Please enter your  last name</div>

                                                                </div>
                                                            </div>


                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <select className="browser-default custom-select">
                                                                        <option>City</option>
                                                                        <option value="1">Option 1</option>
                                                                        <option value="2">Option 1</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div className="col-12">
                                                                <div className="form-group">
                                                                    <select className="browser-default custom-select">
                                                                        <option>State</option>
                                                                        <option value="1">Option 1</option>
                                                                        <option value="2">Option 1</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </MDBTabPane>
                                                    <MDBTabPane tabId="2" role="tabpanel">

                                                        eafff
                                                        </MDBTabPane>

                                                </MDBTabContent>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="grn_btn btn_consent form_button">

                                    <MDBBtn color="primary" type="submit" size="lg" block>
                                        Next ›
                                            </MDBBtn>

                                </div>
                            </div>


                        </form>


                    </div>




                </div >

            </div >
        )
    }
}

export default Address;