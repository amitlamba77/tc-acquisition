import React from 'react';
import './sass/breadcrumb.scss';
import Breadcrumb from 'react-bootstrap/Breadcrumb';

class Breadcrumbs extends React.Component {
    render() {
        return (
            <div className="breadcrumb">
                <Breadcrumb>
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>
                        Current Page
                    </Breadcrumb.Item>
                </Breadcrumb>




            </div >
        )
    }
}

export default Breadcrumbs;
