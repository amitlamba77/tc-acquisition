import React from 'react';

import InfiniteCarousel from 'react-leaf-carousel';
import './sass/premium.scss';

class PremiumMembershipOffer extends React.Component {
    render() {
        var { image, topic, heading } = this.props;
        return (
            <InfiniteCarousel
                breakpoints={[
                    {
                        breakpoint: 500,
                        settings: {
                            slidesToShow: 1.2,
                            slidesToScroll: 1,
                        },
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 0,
                        },
                    }
                ]}
                sidesOpacity={0}
                sideSize={0}
                slidesToScroll={0}
                slidesToShow={3}
                scrollOnDevice={true}
                swipe={true}
                margin={0}
            >
                <div className='item'>
                    <div className="list_offers">
                        <div className="offer_listing">
                            <div className="float-left">
                                <img src={require('./images/' + image)} alt="Premium Offer" />
                            </div>
                            <div className="float-left list_desc">
                                <div className="list_head">{heading}</div>
                                <p>{topic}</p>
                            </div>
                            <div className="clearfix"></div>
                        </div>


                    </div>
                    <div className="list_offers">
                        <div className="offer_listing">
                            <div className="float-left">
                                <img src={require('./images/' + image)} alt="Premium Offer" />
                            </div>
                            <div className="float-left list_desc">
                                <div className="list_head">{heading}</div>
                                <p>{topic}</p>
                            </div>
                            <div className="clearfix"></div>
                        </div>


                    </div>
                    <div className="list_offers">
                        <div className="offer_listing">
                            <div className="float-left">
                                <img src={require('./images/' + image)} alt="Premium Offer" />
                            </div>
                            <div className="float-left list_desc">
                                <div className="list_head">{heading}</div>
                                <p>{topic}</p>
                            </div>
                            <div className="clearfix"></div>
                        </div>


                    </div>
                </div>


                <div className='item'>
                    <div className="list_offers">
                        <div className="offer_listing">
                            <div className="float-left">
                                <img src={require('./images/' + image)} alt="Premium Offer" />
                            </div>
                            <div className="float-left list_desc">
                                <div className="list_head">{heading}</div>
                                <p>{topic}</p>
                            </div>
                            <div className="clearfix"></div>
                        </div>


                    </div>
                    <div className="list_offers">
                        <div className="offer_listing">
                            <div className="float-left">
                                <img src={require('./images/' + image)} alt="Premium Offer" />
                            </div>
                            <div className="float-left list_desc">
                                <div className="list_head">{heading}</div>
                                <p>{topic}</p>
                            </div>
                            <div className="clearfix"></div>
                        </div>


                    </div>
                    <div className="list_offers">
                        <div className="offer_listing">
                            <div className="float-left">
                                <img src={require('./images/' + image)} alt="Premium Offer" />
                            </div>
                            <div className="float-left list_desc">
                                <div className="list_head">{heading}</div>
                                <p>{topic}</p>
                            </div>
                            <div className="clearfix"></div>
                        </div>


                    </div>
                </div>

                <div className='item'>
                    <div className="list_offers">
                        <div className="offer_listing">
                            <div className="float-left">
                                <img src={require('./images/' + image)} alt="Premium Offer" />
                            </div>
                            <div className="float-left list_desc">
                                <div className="list_head">{heading}</div>
                                <p>{topic}</p>
                            </div>
                            <div className="clearfix"></div>
                        </div>


                    </div>
                    <div className="list_offers">
                        <div className="offer_listing">
                            <div className="float-left">
                                <img src={require('./images/' + image)} alt="Premium Offer" />
                            </div>
                            <div className="float-left list_desc">
                                <div className="list_head">{heading}</div>
                                <p>{topic}</p>
                            </div>
                            <div className="clearfix"></div>
                        </div>


                    </div>
                    <div className="list_offers">
                        <div className="offer_listing">
                            <div className="float-left">
                                <img src={require('./images/' + image)} alt="Premium Offer" />
                            </div>
                            <div className="float-left list_desc">
                                <div className="list_head">{heading}</div>
                                <p>{topic}</p>
                            </div>
                            <div className="clearfix"></div>
                        </div>


                    </div>
                </div>




            </InfiniteCarousel>
        )
    }
}



export default PremiumMembershipOffer;