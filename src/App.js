import React from 'react';
import './css/ntb.css';
import './css/plan_css.css';
import './sass/common.scss';
import Home from './Home';
import Membership from './Membership';
import Offers from './Offers';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Mobilenumber from './MobileNumber';
import EnterOTP from './EnterOTP';
import Loader from './Loader';
import PersonalInfo from './PersonalInfo';
import Address from './Address';
import Confirmcard from './Confirmcard';
import Steps from './Steps';
import Sorrypage from './Sorry';
import Aboutus from './Aboutus';
import Partner from './Partner';
import FaqPage from './FAQ';







function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/membership" component={Membership} />
          <Route path="/offers" component={Offers} />
          <Route path="/mobilenumber" component={Mobilenumber} />
          <Route path="/enterotp" component={EnterOTP} />
          <Route path="/loader" component={Loader} />
          <Route path="/personalinfo" component={PersonalInfo} />
          <Route path="/address" component={Address} />
          <Route path="/confirmcard" component={Confirmcard} />
          <Route path="/steps" component={Steps} />
          <Route path="/sorry" component={Sorrypage} />
          <Route path="/aboutus" component={Aboutus} />
          <Route path="/partnerwithus" component={Partner} />
          <Route path="/faq" component={FaqPage} />
        </Switch>
      </div >
    </Router>
  );
}

export default App;
