import React from 'react';
import Navigation from './Navigation';
import Breadcrumbs from './Breadcrumb';
import Footer from './Footer';


class Aboutus extends React.Component {
    render() {

        return (
            <div className="generic_pages">
                <div className="ntb_page offer_page sales_page">
                    <div className="form_plan_new form_plan">
                        <div className="top_card_block">
                            <div className="card_block">
                                <Navigation />
                                <div className="title_header">
                                    <div className="card_container">
                                        <div className="">
                                            <Breadcrumbs />
                                            <h1>About Us</h1>
                                        </div>
                                    </div>
                                </div>
                                <div className="page_content">
                                    <div className="card_container">
                                        <div className="title_content">
                                            <div className="ttl">
                                                Times Card
                                            </div>
                                            <div className="cont">
                                                Times Card is a co-branded credit card offering from Times Internet Limited and HDFC Bank. The credit card is an entertainment oriented card that wants to add more happiness and excitement to your day to day life.
                                            </div>
                                        </div>
                                        <div className="title_content">
                                            <div className="ttl">
                                                About Times Internet
                                            </div>
                                            <div className="cont">
                                                <p>Times Internet, the largest Indian Internet Network, is the digital venture of Times of India Group, India's largest media and entertainment group.</p>

                                                <p>Since its inception in 1999, Times Internet has been at the cutting edge of digital innovation in India. Times Internet operates a portfolio of web and mobile properties that engage millions of users globally. Today, Times Internet is the largest Indian online group in India, according to Comscore, with over 30 million monthly visitors. Its portfolio has expanded from Indiatimes.com, a horizontal internet play, to a wide range of online offerings spanning news, MVAS, e-commerce, email, blogs, music, video, and location based services.</p>

                                                <p>In essence, Times Internet connects with users across the many digital experiences that a consumer might have.</p>

                                                <p>As an Internet company, Times Internet enjoys a significant advantage emanating from its relationship with India's largest media and entertainment house, The Times Group - one of the most respected business houses in the country with a proven track record for media leadership across platforms, spanning 175 years. Know more</p>
                                            </div>
                                        </div>
                                        <div className="title_content">
                                            <div className="ttl">
                                                About HDFC Bank
                                            </div>
                                            <div className="cont">
                                                <p>Promoted in 1995 by Housing Development Finance Corporation (HDFC), India's leading housing finance company, HDFC Bank is one of India's premier banks providing a wide range of financial products and services to its 26 million customers across hundreds of Indian cities using multiple distribution channels including a pan-India network of branches, ATMs, phone banking, net banking and mobile banking. Within a relatively short span of time, the bank has emerged as a leading player in retail banking, wholesale banking, and treasury operations, its three principal business segments.</p>

                                                <p>The bank's competitive strength clearly lies in the use of technology and the ability to deliver world-class service with rapid response time. Over the last 17 years, the bank has successfully gained market share in its target customer franchises while maintaining healthy profitability and asset quality.</p>

                                                <p>In the credit cards space, HDFC Bank has cemented its position as the number one player in the industry. The bank has always believed in offering every Indian a product designed specifically for him or her, and in the recent past it has launched credit cards for women, doctors, teachers, and farmers. Know more</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <Footer className="generic_footer" />
                </div>
            </div>
        )
    }
}

export default Aboutus;