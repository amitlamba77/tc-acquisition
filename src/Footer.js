import React from 'react';
import './sass/footer.scss';
import './sass/common.scss';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class Footer extends React.Component {
    render() {

        return (
            <div className="footer">
                <div className="footer_top"></div>
                <div className="footer_content">
                    <div className="card_container">

                        <div className="save_upto">
                            <img src={require("./images/girl.svg")} alt="Save" className="float-left" />
                            <div className="float-left save_upto_text">
                                <div className="g_text small_text">Save upto</div>
                                <div className="g_text">
                                    ₹60000 <span>/Year</span>
                                </div>
                                <a href="#">SEE HOW</a>
                                <div className="clearfix"></div>
                            </div>
                            <div className="clearfix"></div>
                        </div>

                        <div className="apply_now_right">

                            <Button variant="card_apply">Apply Now ›</Button>
                            <div>FREE FOR THE FIRST YEAR</div>
                        </div>
                        <div className="clearfix"></div>

                    </div>
                    <div className="disclaimer">
                        <div className="card_container">

                            <ol>
                                DISCLAIMER:
                                <li>
                                    You will receive the welcome gift within 6-8 weeks of payment of annual fee and upon spending INR 90,000 in first 3 months of Cardmembership The welcome gift of Taj, SeleQtions and Vivanta Hotels' stay vouchers worth INR 45,000 is applicable effective 1st November, 2019
                                </li>
                                <li>
                                    Terms and Conditions Apply. This represents generic & indicative value based on the average usage by the existing Platinum Cardmembers. It may vary basis the usage.
                                </li>
                                <li>
                                    Subject to terms and conditions. For detailed T&Cs refer here
                                </li>
                                <li>
                                    Average value based on Fine Hotels & Resorts bookings in 2018 for stays of two nights. Actual value will vary based on property, room rate, upgrade availability, and use of benefits.
                                </li>
                                <li>
                                    Certain room categories are not eligible for upgrade
                                </li>
                                <li>
                                    Offers are subject to change from time to time
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div className="footer_social">
                        <div className="card_container">
                            <div className="logo_store">
                                <div className="tc_logo">
                                    <img src={require("./images/tc_logo.svg")} alt="Times card" />
                                </div>
                                <div className="store_icons">
                                    <a href="#" target="_blank">
                                        <img src={require("./images/playstore.png")} alt="Play Store" />
                                    </a>
                                    <a href="#" target="_blank">
                                        <img src={require("./images/appstore.png")} alt="App Store" />
                                    </a>
                                </div>

                            </div>
                            <div className="social_supoort">
                                <div className="social_icons">
                                    <a href="#" target="_blank">
                                        <img src={require("./images/logo-facebook.svg")} alt="Play Store" />
                                    </a>
                                    <a href="#" target="_blank">
                                        <img src={require("./images/logo-twitter.svg")} alt="Play Store" />
                                    </a>
                                    <a href="#" target="_blank">
                                        <img src={require("./images/logo-instagram.svg")} alt="Play Store" />
                                    </a>
                                    <a href="#" target="_blank">
                                        <img src={require("./images/logo-youtube.svg")} alt="Play Store" />
                                    </a>
                                </div>
                                <div className="support">
                                    <div>Need Help?</div>
                                    <div>Write to <span>support@timescard.com</span> or <a href="#" target="_blank">Chat with us</a></div>
                                </div>
                            </div>
                            <div className="clearfix"></div>

                            <div className="address_links">
                                <div className="row">
                                    <div className="col-sm-9">
                                        <div className="links">
                                            <ul>

                                                <li>

                                                    <Link to='/aboutus'>
                                                        About Us
                                                </Link>
                                                </li>
                                                <li>
                                                    <a href="#" target="_blank">Titanium Card</a>
                                                </li>
                                                <li>
                                                    <a href="#" target="_blank">FAQs</a>
                                                </li>

                                                <li>
                                                    <a href="#" target="_blank">Feedback & Complaints</a>
                                                </li>
                                                <li>
                                                    <a href="#" target="_blank">Platinum Card</a>
                                                </li>
                                                <li>
                                                    <a href="#" target="_blank">Terms & Conditions</a>
                                                </li>
                                                <li>
                                                    <a href="#" target="_blank">Partner with us</a>
                                                </li>
                                                <li>
                                                    <a href="#" target="_blank">Times Card Debit</a>
                                                </li>
                                                <li>
                                                    <a href="#" target="_blank">Privacy Policy</a>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-sm-3">
                                        <div className="address">
                                            <div className="head_title">Corporate Office:</div>
                                            Times Internet Ltd. Plot 391,<br />
                                            Phase III, Gurugram,<br />
                                            Haryana, 122016
                                        </div>
                                    </div>
                                </div>
                                <div className="clearfix"></div>
                            </div>




                        </div>
                    </div>
                    <div className="copyright">
                        <div className="card_container">
                            <div className="blog_car">
                                <a href="#" target="_blank">Read Our Blog</a>
                                <a href="#" target="_blank">Careers</a>
                            </div>
                            <div className="copyright_text">
                                &copy; 2020 Times Internet Ltd. All rights reserved.
                            </div>

                            <div className="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>




















        )
    }
}

export default Footer;