import React from 'react';
import './App.css';
import './sass/cardconfirm.scss';
import { MDBBtn } from "mdbreact";


class Steps extends React.Component {

    render() {
        return (

            <div className="blur_bg">
                <div className="ntb_page">
                    <div className="white_common_bg center_box ">
                        <form className="needs-validation"
                            onSubmit={this.submitHandler}
                            noValidate>
                            <div className="form_plan_new form_plan">
                                <div className="head ">
                                    <h1>Next steps...</h1>
                                </div>


                                <div className="card_form_box form_box">
                                    <div class="thanks_page">
                                        <div class="">
                                            <div className="row">
                                                <div className="col-12">
                                                    <img src={require("./images/platinum.svg")} className="img-fluid" alt="Times Card" />
                                                </div>
                                            </div>
                                            <div class="arn_docs">
                                                <div class="tr_head"><span></span> Application Submitted</div>
                                                <div class="cent_cont">You can check the status of your application <a href="www.timescard.com/status">www.timescard.com/status</a> Also we’ve sent an email to you at <span>vineet@gmail.com</span> with all the necessary details.</div>
                                                <div class="tr_head_process">
                                                    <div class="tr_head "><span></span> Pending for Approval</div>
                                                    <div class="cent_cont border_0">Bank executive will get in touch for document collection and address verification.</div>
                                                </div>

                                                <div class="arn_docs_in">
                                                    <div class="cent_cont border_0">Please keep the below documents handy</div>

                                                    <div class="tr_head_process">
                                                        <div class="tr_head ">Identity Proof</div>
                                                        <div class="cent_cont border_0">PAN Card / Driving License / Aadhaar Card / Passport / Voter ID</div>
                                                    </div>
                                                    <div class="tr_head_process">
                                                        <div class="tr_head ">Address Proof</div>
                                                        <div class="cent_cont border_0">Driving License / Aadhaar Card / Passport / Voter ID / Telephone or Electricity Bill / Rent Agreement</div>
                                                    </div>
                                                    <div class="tr_head_process">
                                                        <div class="tr_head ">Income Proof</div>
                                                        <div class="cent_cont border_0">
                                                            &#8226; Salaried- Payslip of last 2 months, Company ID, Bank statement
                                                            &#8226; Self Employed- ITR of last 2 years, Bank statement
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div className="grn_btn btn_consent form_button">

                                    <MDBBtn color="primary" type="submit" size="lg" block>
                                        Submit ›
                                                </MDBBtn>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Steps;