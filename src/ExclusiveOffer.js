import React from 'react';

import InfiniteCarousel from 'react-leaf-carousel';

class ExclusiveOffer extends React.Component {
    render() {
        // var { offerDesc, logoImage, offerName, offerImage } = this.props;
        return (
            <InfiniteCarousel
                breakpoints={[
                    {
                        breakpoint: 500,
                        settings: {
                            slidesToShow: 1.2,
                            slidesToScroll: 1,
                        },
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 0,
                        },
                    }
                ]}
                sidesOpacity={0}
                sideSize={0}
                slidesToScroll={2}
                slidesToShow={2}
                scrollOnDevice={true}
                swipe={true}
                margin={0}
            >

                <div className="item">
                    <div className="welcome_offer_block exclusive_offer_1 rel-pos">
                        <div className="head">
                            <div className="offer_details">

                                <div className="desc">Get 15% off on select styles</div>
                                <div className="logo_offer">
                                    <img src={require('./images/myntra.png')} alt="Exclusive Offer" />
                                </div>
                                <div>🔥 Hottest Offer</div>
                                <div className="clearfix"></div>
                            </div>
                            <div className="logo_right">
                                <img src={require('./images/excluisve_offer1.png')} alt="Exclusive Offer" />
                            </div>
                            <div className="clearfix"></div>
                        </div>

                    </div>
                    <div className="welcome_offer_block exclusive_offer_2 rel-pos">
                        <div className="head">
                            <div className="offer_details">

                                <div className="desc">Get ₹3000 off on Hotels and Int'l Flights!</div>
                                <div className="logo_offer">
                                    <img src={require('./images/eatfit.png')} alt="Exclusive Offer" />
                                </div>
                                <div>Availed 3000 times</div>
                                <div className="clearfix"></div>
                            </div>
                            <div className="logo_right">
                                <img src={require('./images/excluisve_offer3.png')} alt="Exclusive Offer" />
                            </div>
                            <div className="clearfix"></div>
                        </div>

                    </div>


                </div>
                <div className="item">
                    <div className="welcome_offer_block exclusive_offer_3 rel-pos">
                        <div className="head">
                            <div className="offer_details">

                                <div className="desc">Get 15% off on select styles</div>
                                <div className="logo_offer">
                                    <img src={require('./images/yatra.png')} alt="Exclusive Offer" />
                                </div>
                                <div>🔥 Hottest Offer</div>
                                <div className="clearfix"></div>
                            </div>
                            <div className="logo_right">
                                <img src={require('./images/excluisve_offer2.png')} alt="Exclusive Offer" />
                            </div>
                            <div className="clearfix"></div>
                        </div>

                    </div>
                    <div className="welcome_offer_block exclusive_offer_4 rel-pos">
                        <div className="head">
                            <div className="offer_details">

                                <div className="desc">Get ₹3000 off on Hotels and Int'l Flights!</div>
                                <div className="logo_offer">
                                    <img src={require('./images/chaayos.png')} alt="Exclusive Offer" />
                                </div>
                                <div>Availed 3000 times</div>
                                <div className="clearfix"></div>
                            </div>
                            <div className="logo_right">
                                <img src={require('./images/excluisve_offer4.png')} alt="Exclusive Offer" />
                            </div>
                            <div className="clearfix"></div>
                        </div>

                    </div>


                </div>

            </InfiniteCarousel>
        )
    }
}



export default ExclusiveOffer;