import React from 'react';
import './App.css';
import './sass/common.scss';
import './sass/form.scss';


class Loader extends React.Component {
    render() {

        return (
            <div className="blur_bg">
                <div className="ntb_page loader_check">
                    <div className="center_box loader_page">
                        <form action="" method="post">
                            <div className="form_plan_new form_plan">
                                <div className="loader_in">
                                    <div className="loader">
                                        <img src={require("./images/anchorage.svg")} alt="Checking elegibility" />
                                    </div>
                                    <div className="clearfix"></div>
                                    <div className="loader_text">
                                        <h3>Checking elegibility and fetching other details</h3>
                                        <h5>Prefilling details</h5>
                                    </div>
                                </div>
                            </div>

                        </form>


                    </div>




                </div>

            </div >
        )
    }
}

export default Loader;

