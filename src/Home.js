import React from 'react';
import './css/ntb.css';
import './css/plan_css.css';
import './sass/home.scss';
import Navigation from './Navigation';
import TopOffers from './TopOffers';
import WelcomeOffer from './WelcomeOffer';
import ExclusiveOffer from './ExclusiveOffer';
import PremiumMembershipOffer from './PremiumOffer';
import Footer from './Footer';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';



function Home() {
    return (

        <div className="Home">
            <div className="ntb_page offer_page sales_page">
                <div className="form_plan_new form_plan">
                    <div className="top_card_block">
                        <div className="card_block">

                            <Navigation />

                            <div className="left_bg_rect"></div>

                            <div className="card_container">
                                <div className="award_logo">
                                    <div className="et_mobbys">
                                        <img src={require("./images/etnow.svg")} alt="Times card" /><img src={require("./images/mobbys.svg")} alt="Times card" />

                                    </div>
                                    <div className="tcard_block d-block d-sm-none"><img src={require("./images/platinum.svg")} alt="Times card" /></div>
                                    <div className="tcard_block d-none d-sm-block"><img src={require("./images/creditcard2.svg")} alt="Times card" /></div>
                                </div>

                                <div className="offer_logo">
                                    <div className="tc_logo d-none d-sm-block">
                                        <img src={require("./images/tc_logo.svg")} alt="Times card" />
                                    </div>
                                    <div className="offer_txt">
                                        India’s most rewarding Lifestyle Card now has more than
                        <div className="gold_text">20,000 + OFFERS</div>
                                        <div className="small">FREE for the first year</div>
                                        <div className="apply_track">
                                            <Link to='/mobilenumber'>
                                                <Button variant="card_apply">Apply Now ›</Button>
                                            </Link>

                                            <Button variant="track_btn">
                                                TRACK APPLICATION ›
                                        </Button>
                                            <div className="clearfix"></div>
                                        </div>

                                    </div>
                                </div>
                                <div className="clearfix"></div>
                            </div>

                        </div>
                        {/* card block div ends here */}
                        <div className="bottom_part"></div>
                    </div>

                    <div className="top_offer">
                        <div className="card_container">
                            <div className="ttl_offer">
                                TOP OFFERS 🎉
                    <button className="float-right d-none d-sm-block view_all">VIEW ALL ›</button>
                                <div className="clearfix"></div>
                            </div>
                            <div className="list_offers">
                                <TopOffers image="top_1.svg" topic="Get 1 year Times Prime membership worth ₹999" />
                                <TopOffers image="top_2.svg" topic="Get 5% cashback on every dineout transaction" />
                                <TopOffers image="top_3.svg" topic="Free 12 months Gaana Plus subscription worth ₹999" />
                                <TopOffers image="top_4.svg" topic="Free Zomato Gold membership worth ₹750" />
                                <TopOffers image="top_5.svg" topic="Free bigbasket bbstar Subscription for 6 months" />
                                <TopOffers image="top_6.svg" topic="Get 10% discount on all UberGo and Uber Premier rides" />

                                <a href="#" className="more_offer_link">1000+ more offers</a>
                            </div>
                        </div>


                    </div>
                    {/* Top Offer Section Ends Here  */}

                    {/* Welcome Section Starts here */}

                    <div className="welcome_offer">
                        <div className="welcome_upper_bg"></div>
                        <div className="welcome_content">
                            <div className="card_container">
                                <div className="ttl_offer">
                                    Welcome offers worth ₹5000 🎁
                        <button className="float-right d-none d-sm-block view_all">VIEW ALL ›</button>
                                    <div className="clearfix"></div>
                                </div>

                                <div className="welcome_offers welcome_desktop">
                                    <WelcomeOffer />
                                </div>


                                <div className="clearfix"></div>

                                <div className="premium_membership">
                                    <div className="ttl_offer">
                                        Premium Memberships ⭐️
                                    </div>
                                    <PremiumMembershipOffer image="pm1.svg" topic="Enjoy 1 + 1 on food, drinks and buffets" heading="Gourmet Passport by dineout" />

                                </div>

                            </div>
                        </div>
                        <div className="welcome_lower_bg welcome_lower_bg1"></div>
                    </div>

                    {/* exclusive offer */}

                    <div className="excluive_offer">
                        <div className="card_container">
                            <div className="row">
                                <div className="col-sm-12">

                                </div>
                                <div className="col-sm-4">
                                    <div className="ttl_offer">Get exclusive offers from
                                    brands you love ♥️
                                        </div>
                                    <div className="all_exclusive">

                                        <img src={require("./images/all_excluive.png")}
                                            className="all_excluive_img" />
                                    </div>
                                </div>
                                <div className="col-sm-8">
                                    <button className="float-right view_all">VIEW ALL ›</button>
                                    <div className="clearfix"></div>
                                    <div className="exclusive_carousel welcome_offers welcome_desktop">
                                        <ExclusiveOffer />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    {/* How to apply section */}
                    <div className="how_to">
                        <div className="card_container apply_section">
                            <div className="row">
                                <div className="col-sm-3 how_to_apply">
                                    <div className="ttl_offer mar0">How to apply?</div>
                                    <div className="ttl_offer_grey text-left">It takes just 2 minutes.</div>
                                </div>
                                <div className="col-sm-9">
                                    <div className="row bs-wizard">

                                        <div className="col-sm-4 bs-wizard-step complete">

                                            <div className="progress"><div className="progress-bar"></div></div>
                                            <a href="#" className="bs-wizard-dot">
                                                <img src={require("./images/personal.png")} className="img_brder" />
                                            </a>
                                            <div className="bs-wizard-info text-center">
                                                <div>1. Personal Details</div>
                                                <div className="details">Some details</div>
                                            </div>
                                            <div className="clearfix"></div>
                                        </div>

                                        <div className="col-sm-4 bs-wizard-step complete">

                                            <div className="progress"><div className="progress-bar"></div></div>
                                            <a href="#" className="bs-wizard-dot">
                                                <img src={require("./images/profession.png")} className="img_brder" />
                                            </a>
                                            <div className="bs-wizard-info text-center">
                                                <div>2. Professional Details</div>
                                                <div className="details">Some details</div>
                                            </div>
                                            <div className="clearfix"></div>
                                        </div>

                                        <div className="col-sm-4 bs-wizard-step active">

                                            <div className="progress"><div className="progress-bar"></div></div>
                                            <a href="#" className="bs-wizard-dot">
                                                <img src={require("./images/contact_img.png")} className="img_brder" />
                                            </a>
                                            <div className="bs-wizard-info text-center">
                                                <div>3. Contact Details</div>
                                                <div className="details">Some details</div>
                                            </div>
                                            <div className="clearfix"></div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    {/* footer section */}
                    <Footer />




                </div>
            </div >
        </div >

    );
}

export default Home;
