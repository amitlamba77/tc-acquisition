import React from 'react';
import './App.css';
import './sass/cardconfirm.scss';
import { MDBBtn } from "mdbreact";



class Sorrypage extends React.Component {
    render() {

        return (
            <div className="blur_bg">
                <div className="ntb_page">
                    <div className="white_common_bg center_box ">
                        <form className="needs-validation"
                            onSubmit={this.submitHandler}
                            noValidate>
                            <div className="form_plan_new form_plan">
                                <div class="card_form_box form_box sorry_page">
                                    <div class="sorry_box h-100">
                                        <div className="row h-100">
                                            <div className="col-12 my-auto">
                                                <img src={require("./images/sorry.svg")} className="img-fluid" alt="Sorry" />

                                                <div class="sorry_cont">
                                                    <h3>Opps!</h3>
                                                    <div>Currently you’re not eligible for Times Card Credit card</div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div className="grn_btn btn_consent form_button">

                                    <MDBBtn color="primary" type="submit" size="lg" block>
                                        GO TO HOME ›
                                    </MDBBtn>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        )
    }
}

export default Sorrypage;